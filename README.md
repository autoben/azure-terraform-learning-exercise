


## Terraform in Azure
This repository represents a standard stage by stage implementation of Terraform in Azure, although created as a learning exercise, each pull request into master should be an encapsulated feature addition; for example, implementing remote state, creating a Terraform module etc.

## Status of Project
This project has been put on hold in lieu of other pieces of work, currently the infrastructure is done but the following needs doing:
* Complete web app provisioning code
* Add variables to relevant config
* Clean up and standardise code
* Implement second environment

If you wish to pick up this piece of work or start from fresh, please let Ben Ashton or Owen Tuz know and we can give you the relevant access / starter pitch.

### The solution
Keeping to the goal of the learning exercise; learning infrastructure as code using Terraform in Azure - the application is fairly simple and nearly all of the solution is based on the infrastructure. The solution requirements are as follows:

* A simple web application printing the private IP and hostname of the underlying host.
* The web application should be accessible from the Waterloo office only
* The web application should be load balanced across multiple hosts
* The infrastructure hosting the web application should scale based on load.
* There should be a development and production environment.
* Automated testing of the infrastructure should be possible in the development environment


### Prerequisites
This solution and all work done associated with it has been done on Mac, below are the versions of used tooling:

* Terraform - `version : v0.12.21` - this can be installed through [Homebrew](https://brew.sh/)
* Ansible - `version : 2.9.3` - this can be installed through [Homebrew](https://brew.sh/)
* Azure access

### Authenticating with Azure
In order to run Terraform in Azure, you must first authenticate - this solution uses [service principals](https://docs.microsoft.com/en-us/azure/active-directory/develop/app-objects-and-service-principals) to do so. Following [this guide](https://www.terraform.io/docs/providers/azurerm/guides/service_principal_client_certificate.html) will set up everything needed, the environment variable method has been used thus far.

### Creating Terraform remote state in Azure
[Remote states](https://www.terraform.io/docs/state/remote.html) make using Terraform within a team much easier. In order to use this feature of Terraform (required by this solution at time of writing) a few manual steps must be done in Azure. Following [this guide](https://docs.microsoft.com/en-us/azure/terraform/terraform-backend) will setup nearly everything you need. Use the following configuration when following the guide:


```
# Replace ${env} with the name of your environment (dev/prod)
# This goes in environments/${env}/main.tf
# Ensure that these values are also used to create the Azure resources

storage_account_name = azure-training-storage
container_name       = tfstate
key                  = ${env}.terraform.tfstate
```

## Learning exercise section

### Goal of the learning exercise
The goal of the learning exercise used to create this repository is three part:

* Improve Azure knowledge
* Improve Terraform knowledge
* Following development best practice when using Terraform in Azure

The 'finished product' should fit the following criteria:

* A substantial piece of infrastructure using Terraform fulfilling the following:
	* Auto scaling through load
	* Dev and Prod environment
	* Fully IaC (no manual steps)
* Ansible provisioning in place for application hosted on the Infrastructure
* Infrastructure testing in place using Terratest

### Deliverables
This section contains high level functionality that the final solution should contain and its current status, the points are not ordered and can be a number of commits/pull requests. This list is dynamic and can change throughout the learning exercise.

* ~~Setup Terraform Azure authentication~~
* ~~Implement Azure Terraform backend remote state~~
* ~~Template out authentication script~~
* ~~Document how to use authentication script and the values needed~~
* ~~Document manual steps needed (adding user to Azure Subscription, creating Storage Account for remote state at time of writing)~~
* Document how to use solution
* ~~VNet/Subnet/AZ Terraform~~
* ~~Virtual Host Terraform~~
* ~~Load balancing Terraform~~
* Auto scaling Terraform
* Web application provisioning
* ~~Infrastructure testing~~

### Optional Deliverables
This section contains functionality that could be added but does not directly map to the goal of the learning exercise:

* Implement visit count onto the web app
* Infrastructure/solution diagram created

### How to contribute
If you want to partake in the learning exercise or change the code post learning exercise, please message Ben Ashton for Contributor permission on this repo.
