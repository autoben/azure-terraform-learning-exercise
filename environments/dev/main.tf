provider "azurerm" {
  # Whilst version is optional, we /strongly recommend/ using it to pin the version of the Provider being used
  version = "=2.0.0"
  features {}
}

terraform {
  backend "azurerm" {
    storage_account_name = "azuretrainingstorageal"
    container_name       = "tfstate"
    key                  = "benvinit.terraform.tfstate"
  }
}

resource "azurerm_resource_group" "example" {
  name     = "acceptanceTestResourceGroup1"
  location = "West US"
}

resource "azurerm_network_security_group" "example" {
  name                = "acceptanceTestSecurityGroup1"
  location            = azurerm_resource_group.example.location
  resource_group_name = azurerm_resource_group.example.name
}

resource "azurerm_network_security_rule" "allowInboundSsh" {
  name                        = "allowInboundSshRule"
  priority                    = 1000
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "Tcp"
  source_port_range           = "*"
  destination_port_range      = "22"
  source_address_prefix       = "77.108.144.128/29"
  destination_address_prefix  = "*"
  resource_group_name         = azurerm_resource_group.example.name
  network_security_group_name = azurerm_network_security_group.example.name
}

resource "azurerm_network_security_rule" "allowInboundHttp" {
  name                        = "allowInboundHttpRule"
  priority                    = 2000
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "Tcp"
  source_port_range           = "*"
  destination_port_range      = "80"
  source_address_prefix       = "77.108.144.128/29"
  destination_address_prefix  = "*"
  resource_group_name         = azurerm_resource_group.example.name
  network_security_group_name = azurerm_network_security_group.example.name
}

resource "azurerm_virtual_network" "example" {
  name                = "virtualNetwork1"
  location            = azurerm_resource_group.example.location
  resource_group_name = azurerm_resource_group.example.name
  address_space       = ["10.0.0.0/16"]

  subnet {
    name           = "subnet1"
    address_prefix = "10.0.1.0/24"
    security_group = azurerm_network_security_group.example.id
  }

  subnet {
    name           = "subnet2"
    address_prefix = "10.0.2.0/24"
    security_group = azurerm_network_security_group.example.id
  }

  subnet {
    name           = "subnet3"
    address_prefix = "10.0.3.0/24"
    security_group = azurerm_network_security_group.example.id
  }

  tags = {
    environment = "dev"
  }
}

resource azurerm_linux_virtual_machine_scale_set "example" {
  name                = "webScaleSet1"
  resource_group_name = azurerm_resource_group.example.name
  location            = azurerm_resource_group.example.location
  depends_on = [
    azurerm_virtual_network.example,
    azurerm_lb_rule.example,
  ]

  tags = {
    environment = "dev"
  }

  sku       = "Standard_B1s"
  instances = 2

  source_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "18.04-LTS"
    version   = "latest"
  }

  os_disk {
    storage_account_type = "Standard_LRS"
    caching              = "ReadWrite"
  }

  admin_username = "exampleadmin"

  # This would normally be the public portion of a shared key but for the
  # purposes of this exercise, add your own
  # You can also add multiple admin_ssh_key blocks within a scaleset
  admin_ssh_key {
    username = "exampleadmin"
    # Azure will only accept RSA keys at least 2048 bits in size
    public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCemjVzG7rukWTuLlglMEIcrECg9/l6pZLPVEZnmdFCTOYZjxswV2W4J2uygHEHkXbeuxLVjpSM3+QTfUmd/D92z80NSjiMTMAAdDO97VSVY47njm5jDFBKpdOPBl0aOXFoj8oiK6Di7JiYDfUV/z5zqINjCyqh0K9rqO6v58H9s6yMwkXmFNLcPtz3CwB25pbEkZ+xBAZiWqNC6SX90FV5t8c6ht3kwVVAplE4vZa7v0FYEw9FmBOzBBdVsr95NMUpC35V/06HoLhEPdCebKrdaTgdRk6EdNfsI2bVbtCvFQkK0UTPvzziS7pWmrOHizuIl6LZh1x+9uxvcCNp+lAml9DDZIos9rZGOi6+ZicX02jwaXMjpi1a1vDfWXmtaZHbbXGFpYdjUlwaa4Ke8CSQlfbeFf4huyFwAYS2H+pzZJZtE/HR6IuSubEydAXIcSR+MQrD+ZAClzqaczxZZYEMfM97GLLKQhzQKsp2jbwqkg6krGzd0BopD5YdmuM1KE5fplTgP7somDehYFOtgosGs5vs9sor3CYZ866DrW7tMzoPX2ECK0OkonXVEWwgxysKoADMCTgJHutaSqM4HBiSU8yJcLQbEM8z+ydtNzggVw5Zvy9UtgZwtPWYFtak4DG07bpqsJvhKbZbRPvQ6YFcyXtySMuNsXGH+FTB2StHcQ== example_ssh_pubkey"
  }

  admin_ssh_key {
    username   = "exampleadmin"
    public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDOFaO7IG77wLQtxUHTLk0PmfJzatIVmv6gN79Ocazmb5F1zpg8CB4IbuYZuAx8k6OhG4nsBowKWlEbBg0JmXFIuzv+L8Kin+OVnoeruw+/OoAwgo2xdq8Kc57ld2GGQlQWlgnlQ6iOZuTkQcs9hXSctlyd+hA8ouMrxeRCPGaP6sI+zs0orOCpRiSxxsVgA86dvoUGDrq54GQkKWgDyMqKmSKwPY5XpEct19x+IX2NuLJtPr1lyHlO3Fjejkm2Izf2UgXBRLpboVbCZIxT7MXPGQHUGnQ4lw6udRMf480JpzcJ4k6qBrkjuCysyQ2ORmz0mV8wj4UCUwRx0dNz/uvr benashton@Bens-MacBook-Pro.local"
  }

  network_interface {
    name    = "example"
    primary = true

    ip_configuration {
      name    = "vmIpConfiguration"
      primary = true

      # The 'subnet' item can be created as a separate resource and referenced both here and in the VNet config
      # That's much neater, but for learning purposes: we can also do it with a loop and conditional check
      subnet_id = [for subnet in azurerm_virtual_network.example.subnet : subnet.id if subnet.name == "subnet1"][0]

      public_ip_address {
        name = "vmPublicIpAddress"
      }

      load_balancer_backend_address_pool_ids = [
        azurerm_lb_backend_address_pool.example.id,
      ]
    }
  }
}

resource "azurerm_monitor_autoscale_setting" "example" {
  name                = "webAutoscalePolicy"
  enabled             = true
  location            = azurerm_resource_group.example.location
  resource_group_name = azurerm_resource_group.example.name
  target_resource_id  = azurerm_linux_virtual_machine_scale_set.example.id

  profile {
    name = "fixedPoolSize"
    capacity {
      default = 2
      minimum = 2
      maximum = 2
    }
  }
}

resource "azurerm_public_ip" "example" {
  name                = "loadBalancerPublicIPAddress"
  location            = azurerm_resource_group.example.location
  resource_group_name = azurerm_resource_group.example.name
  allocation_method   = "Static"
}

resource "azurerm_lb" "example" {
  name                = "frontendLoadBalancer1"
  location            = azurerm_resource_group.example.location
  resource_group_name = azurerm_resource_group.example.name

  frontend_ip_configuration {
    name                 = "loadBalancerFrontendIPConfiguration"
    public_ip_address_id = azurerm_public_ip.example.id
  }
}

resource "azurerm_lb_backend_address_pool" "example" {
  resource_group_name = azurerm_resource_group.example.name
  loadbalancer_id     = azurerm_lb.example.id
  name                = "webServerBackendAddressPool"
}

resource "azurerm_lb_probe" "example" {
  name                = "httpLbProbe"
  resource_group_name = azurerm_resource_group.example.name
  loadbalancer_id     = azurerm_lb.example.id
  protocol            = "Http"
  port                = 80
  request_path        = "/"
}

resource "azurerm_lb_rule" "example" {
  name                           = "loadBalancerAllowHttpRule"
  loadbalancer_id                = azurerm_lb.example.id
  resource_group_name            = azurerm_resource_group.example.name
  protocol                       = "Tcp"
  frontend_port                  = 80
  backend_port                   = 80
  frontend_ip_configuration_name = "loadBalancerFrontendIPConfiguration"
  backend_address_pool_id        = azurerm_lb_backend_address_pool.example.id
  probe_id                       = azurerm_lb_probe.example.id
}

output "vnet" {
  value = azurerm_virtual_network.example.name
}
