//+ build unit
package test

import (
	"testing"
)

func TestUnit_Vnet(t *testing.T) {
	t.Parallel()
	plan := terraformPlan(t)

	assertResourceExistsInPlan(t, plan, "azurerm_virtual_network", "example")

	assertResourceHasAttribute(t, plan,
		"azurerm_virtual_network", "example",
		"name", "virtualNetwork1")
	assertResourceHasAttribute(t, plan,
		"azurerm_virtual_network", "example",
		"location", "westus")

	expectedSubnet1 := map[string]string{
		"name":           "subnet1",
		"address_prefix": "10.0.1.0/24",
	}
	expectedSubnet2 := map[string]string{
		"name":           "subnet2",
		"address_prefix": "10.0.2.0/24",
	}
	expectedSubnet3 := map[string]string{
		"name":           "subnet3",
		"address_prefix": "10.0.3.0/24",
	}
	assertResourceHasAttribute(t, plan,
		"azurerm_virtual_network", "example",
		"subnet", expectedSubnet1)
	assertResourceHasAttribute(t, plan,
		"azurerm_virtual_network", "example",
		"subnet", expectedSubnet2)
	assertResourceHasAttribute(t, plan,
		"azurerm_virtual_network", "example",
		"subnet", expectedSubnet3)
}
