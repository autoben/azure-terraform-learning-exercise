package test

import (
	"fmt"
	"github.com/gruntwork-io/terratest/modules/terraform"
	"github.com/hashicorp/terraform/plans"
	"github.com/hashicorp/terraform/plans/planfile"
	"github.com/zclconf/go-cty/cty"
	"github.com/zclconf/go-cty/cty/gocty"
	"io/ioutil"
	"os"
	"strconv"
	"testing"
)

// Default set of options for Terraform
func getTerraformOptions() *terraform.Options {
	return &terraform.Options{
		TerraformDir: "../environments/dev",
	}
}

// Runs `terraform init` and `terraform plan` with the default test options
// Returns a Plan object which can be used to assert that plan output matches
// desired state
//
// Prefer using this for unit tests as it does not create any actual resources
func terraformPlan(t *testing.T) plans.Plan {
	tfOptions := getTerraformOptions()
	terraform.Init(t, tfOptions)

	planFile, err := ioutil.TempFile("", "tfTestPlan")
	if err != nil {
		t.Fatal(err)
	}
	// Closing the file looks a bit weird, but Terraform will open and write
	// the plan to it. Creating with TmpFile gives us a cheap way to receive a
	// random filename while also checking that the location is writable
	planFile.Close()

	pathToPlanFile := planFile.Name()
	defer os.Remove(pathToPlanFile)

	terraform.RunTerraformCommand(
		t,
		tfOptions,
		terraform.FormatArgs(tfOptions, "plan", "-out="+pathToPlanFile)...,
	)

	planOutput, err := planfile.Open(pathToPlanFile)
	if err != nil {
		t.Fatal(err)
	}
	plan, err := planOutput.ReadPlan()
	if err != nil {
		t.Fatal(err)
	}
	return *plan
}

// Takes a plan object and asserts that the specified resource will be created
// Fails the test if not found. Otherwise no return value
// Arguments:
// - testing.T from the calling test
// - plan, eg as provided by terraformPlan() from this module
// - tfResourceType: the type of Terraform resource (eg 'aws_instance')
// - tfResourceName: the local name of the resource block in Terraform (eg 'webserver1')
func assertResourceExistsInPlan(
	t *testing.T,
	plan plans.Plan,
	tfResourceType string,
	tfResourceName string,
) {

	resourceIdentifier := fmt.Sprintf("%s.%s", tfResourceType, tfResourceName)
	for _, res := range plan.Changes.Resources {
		if fmt.Sprintf("%s", res.Addr.Resource) == resourceIdentifier {
			return
		}
	}
	t.Fatalf("Expected resource %s was not found in plan output", resourceIdentifier)
}

// Takes a plan object and asserts that the specified resource has some expected attribute
// Fails the test if not found. Otherwise no return value
// Arguments:
// - testing.T from the calling test
// - plan, eg as provided by terraformPlan() from this module
// - tfResourceType: the type of Terraform resource (eg 'aws_instance')
// - tfResourceName: the local name of the resource block in Terraform (eg 'webserver1')
// - attributeName: string name of the resource attribute under test
// - attributeValue: expected value of the attribute under test. This can be a
//                   string for simple key/value tests or map for more complex
//                   structures
func assertResourceHasAttribute(
	t *testing.T,
	plan plans.Plan,
	tfResourceType string,
	tfResourceName string,
	attributeName string,
	attributeExpectedValue interface{},
) {
	resourceIdentifier := fmt.Sprintf("%s.%s", tfResourceType, tfResourceName)

	switch attributeExpectedValue.(type) {
	case string:
		// string is acceptable
	case map[string]string:
		// map of string key/value pairs is acceptable
	default:
		// otherwise throw an error
		t.Fatalf("Not implemented: expected attribute value should be string or map[string]string")
	}

	for _, res := range plan.Changes.Resources {
		if fmt.Sprintf("%s", res.Addr.Resource) == resourceIdentifier {
			attribute, err := getAttributeValueFromPlanOutput(res, attributeName)
			if err != nil {
				t.Fatal(err)
			}
			switch attribute.(type) {
			default:
				t.Fatalf("Not implemented: returned value should be string or []map[string]string")
			case string:
				if !(attributeExpectedValue == attribute) {
					t.Fatalf(
						"Attribute '%s' did not match expected value. Expected %s, got %s",
						attributeName,
						attributeExpectedValue,
						attribute,
					)
				}
			case []map[string]string:
				foundMatch := false
				// we need to assert types before we use these values, to
				// prevent the type system getting upset about interface{}
				received := attribute.([]map[string]string)
				expected := attributeExpectedValue.(map[string]string)
				for _, item := range received {
					if isSubset(expected, item) {
						foundMatch = true
						break
					}
				}
				if !foundMatch {
					t.Fatalf(
						"Attribute '%s' did not contain expected key/value pairs. Expected to see %v in one of %v",
						attributeName,
						attributeExpectedValue,
						attribute,
					)
				}
			}
		}
	}
}

// Gets the value of a resource attribute from the output of `terraform plan`
//
// For simple key/value attributes, returns a string
//
// More complex attributes (such as subnets in an Azure Vnet, of which there can
// be multiple within one resource) are returned as a list of maps with string
// key/value pairs.
func getAttributeValueFromPlanOutput(
	planResource *plans.ResourceInstanceChangeSrc,
	attributeName string,
) (interface{}, error) {
	// Terraform uses the "cty" library to implement dynamic typing:
	//  https://github.com/zclconf/go-cty
	//
	// It's possible to parse this by importing the provider and asking for a
	// schema, but not lightweight as the helper libraries require every single
	// provider is imported - and is prone to breaking if any provider is
	// pinned at a bad version.
	//
	// To work around this, we make heavy use of cty's built-in type assertions
	impliedType, err := planResource.ChangeSrc.After.ImpliedType()
	if err != nil {
		return nil, err
	}
	changeSet, err := planResource.ChangeSrc.After.Decode(impliedType)
	if err != nil {
		return nil, err
	}
	if !changeSet.Type().HasAttribute(attributeName) {
		return nil, fmt.Errorf("Could not find expected attribute with name %s", attributeName)
	}
	attribute := changeSet.GetAttr(attributeName)
	if attribute.Type().IsPrimitiveType() {
		return getStringRepresentationFromCtyValue(attribute)
	} else if attribute.Type().IsTupleType() {
		// Nested attributes are stored as cty Object types
		// so we need to walk the tree and look for matching types
		var objects []map[string]string
		_ = cty.Walk(attribute,
			func(path cty.Path, value cty.Value) (bool, error) {
				if value.Type().IsObjectType() {
					attributeAsMap := make(map[string]string)
					it := value.ElementIterator()
					for it.Next() {
						// Iterate over the elements in the object and try to
						// convert them to string
						key, value := it.Element()
						valueAsString, err := getStringRepresentationFromCtyValue(value)
						// Silently drop any values that fail to convert
						// - these may be empty or dynamic values
						// (ie not known at compile time)
						if err == nil {
							attributeAsMap[key.AsString()] = valueAsString
						}
					}
					objects = append(objects, attributeAsMap)
				}
				return true, nil
			},
		)
		return objects, nil
	} else if !attribute.IsKnown() {
		err := fmt.Errorf(
			"Value of attribute %s cannot be tested as it is not known at compile time and does not appear in plan output",
			attributeName,
		)
		return nil, err
	} else {
		panic(fmt.Sprintf("Not implemented: unable to parse attribute %s of type %s", attributeName, attribute.Type()))
	}
	// "This should never happen" (but we need a return statement here anyway)
	return nil, fmt.Errorf("Unable to parse requested attribute %s", attributeName)
}

// Takes a cty.Value and tries to return a string representation of its contents
// Returns error if cannot parse, or if the value is not known at compile time
func getStringRepresentationFromCtyValue(value cty.Value) (string, error) {
	switch value.Type().FriendlyName() {
	case "string":
		return value.AsString(), nil
	case "bool":
		boolAsString := fmt.Sprintf("%t", value.True())
		return boolAsString, nil
	case "number":
		var valueAsInt int
		err := gocty.FromCtyValue(value, &valueAsInt)
		return strconv.Itoa(valueAsInt), err
	case "dynamic":
		return "DYNAMIC_VALUE", fmt.Errorf("Attribute is not known at compile time")
	default:
		return "", fmt.Errorf("Could not parse attribute to string: %+v", value)
	}
}

// Tests if candidateSet is a subset of parentSet and returns boolean
// where both candidateSet and parentSet are maps of string key/value pairs
func isSubset(candidateSet map[string]string, parentSet map[string]string) bool {
	for key, value := range candidateSet {
		if parentSet[key] != value {
			return false
		}
	}
	return true
}
