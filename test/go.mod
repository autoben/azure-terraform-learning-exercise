module azure-terraform-learning-exercise

go 1.14

require (
	github.com/gruntwork-io/terratest v0.25.2
	github.com/hashicorp/terraform v0.12.21
	github.com/zclconf/go-cty v1.3.1
)
