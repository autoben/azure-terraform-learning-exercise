Test Framework
==============

This directory contains tests for the Terraform code and the final output.

Tests have been written using Golang's [testing](https://golang.org/pkg/testing/) package and [https://github.com/gruntwork-io/terratest](Terratest) as a driver for Terraform.

Prerequisites
=============

- You will need Go >= 1.14 installed, either from your OS's package manager or from https://golang.org/doc/install
- You must have all the prerequisites described in the [main repository README](../README.md).
- Tests run `terraform plan` so you will need your environment set up for remote Terraform state as described in the main README

Running
=======
At present the repo only contains unit tests, which run `terraform plan` and make assertions about the output. These are useful for quick local development as they do not create any resources in Azure.

In future, we would like to add integration tests as part of this exercise. These should create resources in Azure and directly test the user experience (are web resources available, and so on).

To run all tests:

```
go test
```

Unit and integration tests are tagged with [build constraints](https://golang.org/pkg/go/build/) so that you can run individual sets of tests if needed:

```
go test -tags=unit
go test -tags=integration
```

Adding tests
============

Test function names should begin with `TestUnit` or `TestIntegration` and files containing tests must end in `_test.go` in order to be discovered by `go test`.

Please add build tags as described above - either `//+ build unit` or `//+ build integration` at the top of the file.
