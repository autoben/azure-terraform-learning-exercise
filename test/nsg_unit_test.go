//+ build unit
package test

import (
	"testing"
)

func TestUnit_NetworkSecurityGroup(t *testing.T) {
	t.Parallel()
	plan := terraformPlan(t)

	assertResourceExistsInPlan(t, plan, "azurerm_network_security_group", "example")

	assertResourceHasAttribute(t, plan,
		"azurerm_network_security_group", "example",
		"name", "acceptanceTestSecurityGroup1")
	assertResourceHasAttribute(t, plan,
		"azurerm_virtual_network", "example",
		"location", "westus")

	assertResourceExistsInPlan(t, plan, "azurerm_network_security_rule", "allowInboundSsh")
	assertResourceExistsInPlan(t, plan, "azurerm_network_security_rule", "allowInboundHttp")

	assertResourceHasAttribute(t, plan,
		"azurerm_network_security_rule", "allowInboundSsh",
		"destination_port_range", "22")

	assertResourceHasAttribute(t, plan,
		"azurerm_network_security_rule", "allowInboundHttp",
		"destination_port_range", "80")

	alOfficeIpRange := "77.108.144.128/29"

	assertResourceHasAttribute(t, plan,
		"azurerm_network_security_rule", "allowInboundSsh",
		"source_address_prefix", alOfficeIpRange)

	assertResourceHasAttribute(t, plan,
		"azurerm_network_security_rule", "allowInboundHttp",
		"source_address_prefix", alOfficeIpRange)
}
