//+ build unit
package test

import (
	"testing"
)

func TestUnit_ResourceGroup(t *testing.T) {
	t.Parallel()
	plan := terraformPlan(t)

	assertResourceExistsInPlan(t, plan, "azurerm_resource_group", "example")
	assertResourceHasAttribute(t, plan, "azurerm_resource_group", "example",
		"name", "acceptanceTestResourceGroup1")
}
