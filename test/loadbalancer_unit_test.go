//+ build unit
package test

import (
	"testing"
)

func TestUnit_Loadbalancer(t *testing.T) {
	t.Parallel()
	plan := terraformPlan(t)

	assertResourceExistsInPlan(t, plan, "azurerm_lb", "example")

	assertResourceHasAttribute(t, plan,
		"azurerm_lb", "example",
		"name", "frontendLoadBalancer1")

	assertResourceExistsInPlan(t, plan, "azurerm_lb_rule", "example")

	assertResourceHasAttribute(t, plan,
		"azurerm_lb_rule", "example",
		"name", "loadBalancerAllowHttpRule")

	assertResourceHasAttribute(t, plan,
		"azurerm_lb_rule", "example",
		"protocol", "Tcp")

	assertResourceHasAttribute(t, plan,
		"azurerm_lb_rule", "example",
		"frontend_port", "80")

	assertResourceHasAttribute(t, plan,
		"azurerm_lb_rule", "example",
		"backend_port", "80")

	assertResourceExistsInPlan(t, plan,
		"azurerm_lb_backend_address_pool", "example")

	assertResourceHasAttribute(t, plan,
		"azurerm_lb_backend_address_pool", "example",
		"name", "webServerBackendAddressPool")

	assertResourceExistsInPlan(t, plan, "azurerm_lb_probe", "example")

	assertResourceHasAttribute(t, plan,
		"azurerm_lb_probe", "example",
		"protocol", "Http")

	assertResourceHasAttribute(t, plan,
		"azurerm_lb_probe", "example",
		"request_path", "/")
}
