//+ build unit
package test

import (
	"testing"
)

func TestUnit_Scaleset(t *testing.T) {
	t.Parallel()
	plan := terraformPlan(t)

	assertResourceExistsInPlan(t, plan, "azurerm_linux_virtual_machine_scale_set", "example")

	assertResourceHasAttribute(t, plan,
		"azurerm_linux_virtual_machine_scale_set", "example",
		"name", "webScaleSet1")

	assertResourceHasAttribute(t, plan,
		"azurerm_linux_virtual_machine_scale_set", "example",
		"disable_password_authentication", "true")

	disk_settings := map[string]string{
		"storage_account_type": "Standard_LRS",
	}
	assertResourceHasAttribute(t, plan,
		"azurerm_linux_virtual_machine_scale_set", "example",
		"os_disk", disk_settings)

	assertResourceExistsInPlan(t, plan, "azurerm_monitor_autoscale_setting", "example")

	assertResourceHasAttribute(t, plan,
		"azurerm_monitor_autoscale_setting", "example",
		"enabled", true)
}
