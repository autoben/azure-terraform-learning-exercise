provider "azurerm" {
  # Whilst version is optional, we /strongly recommend/ using it to pin the version of the Provider being used
  version = "=2.0.0"
  features {}
}

terraform {
  backend "azurerm" {
    storage_account_name = "benvinitstorage"
    container_name       = "tfstate"
    key                  = "benvinit.terraform.tfstate"
  }
}
